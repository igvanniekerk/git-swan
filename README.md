**Welcome to Git-Swan**

Git-Swan is a simple but fun project that allows you to search for Git-Repositories.

The Site is Built in [Angular Js](https://angularjs.org/), javascript and uses [D3](https://d3js.org/)

Hope you enjoy as much as i did making it.