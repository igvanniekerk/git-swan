'use strict';

myApp.controller('View1Ctrl', ['$scope', '$location', 'gitHubRepoLookup', function($scope, $location, gitHubRepoLookup) {
    // searches for the repos
    $scope.getRepoList = function (repoNameSearch) {
        $scope.showStats = true;
        $scope.data = '';
        gitHubRepoLookup.lookupRepo($scope.repoName).then(onLookupComplete, onError);
    }
    // forwards the user to look at the selected repo
    $scope.openRepo = function (item) {
        $location.path('/repoSummaryView/'+ item);
    }

    var onLookupComplete = function(response) {
        $scope.data = response.data;
        $scope.status = response.status;
        d3.select(".chart").selectAll("div").remove();
        d3Charts();
    };

    var onError = function(reason) {
        $scope.error = "Ooops, something went wrong..";
    };

    // d3 charting tool, would love to play with this more
    var d3Charts = function () {
        var allScores =[];
        for (var i = 0; i < $scope.data.items.length; i++) {
            allScores.push($scope.data.items[i].score);
        }

        d3.select(".chart")
            .selectAll("div")
            .data(allScores)
            .enter().append("div")
            .style("width", function(d) { return d * 10 + "px"; })
            .text(function(d) { return d; });
    };
    // hide and show of buttons on the display, toggles between the chart and the repos
    $scope.statsOrRepos = function () {
        if($scope.showStats == true){
            $scope.showStats = false;
            $scope.showCards = true;
        }else{
            $scope.showStats = true;
            $scope.showCards = false;
        }
    }

    var init = function () {
        $scope.showStats = false;
        $scope.showCards = false;
    }

}]);
