'use strict';




myApp.controller('View2Ctrl', ['$scope', '$routeParams', 'gitHubRepoLookup', function($scope, $routeParams, gitHubRepoLookup) {

    $scope.getComments = function (issue) {
       return gitHubRepoLookup.issueLookup(issue);
    }
    // repo owner api call
    var repoInfoLookupComplete = function(repoInfo) {
        console.log(repoInfo.data);
        $scope.repoInfo = repoInfo.data;
    };

    // should there be any problems  an error message is captured
    var onError = function(repoInfo) {
        $scope.error = "Ooops, something went wrong..";
    };

    // success funtion for all issues
    var repoIssuesLookupComplete = function(issues) {
        console.log(issues);
        $scope.issues = issues.data;
    };
     // fail funtion for all issues
    var onrepoIssuesError = function(issues) {
        $scope.error = "Ooops, something went wrong..";
    };

    // initial setup with two service calls to populate the front end
    var init = function () {

        var ownerName= $routeParams.ownerName;
        var repoName= $routeParams.repoName;

        gitHubRepoLookup.repoInfo(ownerName, repoName).then(repoInfoLookupComplete, onError);
        gitHubRepoLookup.repoIssues(ownerName, repoName).then(repoIssuesLookupComplete, onrepoIssuesError);
    };

    init();
}]);
