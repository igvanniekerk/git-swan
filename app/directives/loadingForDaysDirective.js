/**
 * Created by ignatius on 2016/11/13.
 */

// small but unused directive for a spinner is loading takes days
myApp.directive('loadingForDaysDirective', [ function() {
    return{
        templateUrl: 'views/loadingForDaysView.html'
    };
}]);
