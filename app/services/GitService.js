/**
 * Created by ignatius on 2016/11/13.
 */

// services that deal with api calls
myApp.factory('gitHubRepoLookup', function($http) {
    return {
        lookupRepo: function(repoName) {

            // searches for repositories by name and ajax call that gets the api respons in json
            return  $http.get('https://api.github.com/search/repositories?q='+ repoName)
                .then(function(response) {
                        return response;
                    },
                    // error handler
                    function(response) {
                        return response;
                    });
        },

        // searches for the repo owner and details
        repoInfo: function(ownerName, repoName) {
            return  $http.get('https://api.github.com/search/repositories?q=repo:'+ ownerName+'/'+repoName)
                .then(function(respons) {
                        return respons;
                    },
                    // error handler
                    function(respons) {
                        return respons;
                    });
        },
        // searches for the specific repo and its comments , returns all issues and pull requests
        repoIssues: function(ownerName, repoName) {
            return  $http.get('https://api.github.com/search/issues?q=repo:'+ ownerName+'/'+repoName+'/comments')
                .then(function(response) {
                        return response;
                    },
                    // error handler
                    function(response) {
                        return response;
                    });
        }
    }
});