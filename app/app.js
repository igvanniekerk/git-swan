'use strict';

// Declare app level module which depends on views, and components
var myApp = angular.module('myApp', [
          'ngRoute'
        ]).

// config file responsible for routing to controllers and defualt fallback
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider
      .when('/repoListView', {
          templateUrl: 'views/repoListView.html',
          controller: 'View1Ctrl'
      })
      .when('/repoSummaryView/:ownerName/:repoName', {
          templateUrl: 'views/repoSummaryView.html',
          controller: 'View2Ctrl'
      })
      .otherwise({redirectTo: '/repoListView'});
}]);
